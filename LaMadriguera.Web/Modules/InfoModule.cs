﻿using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LaMadriguera.Web.Modules
{
    public class InfoModule : ModuleBase<SocketCommandContext>
    {
        // Dice hola mundo.
        [Command("hola")]
        [Summary("Replica un mensaje.")]
        public Task HelloAsync([Remainder][Summary("El texto que replica.")] string echo) => ReplyAsync(echo);

        // ReplyAsync es un metodo en ModuleBase.
    }

    [Group("ejemplo")]
    public class SampleModule : ModuleBase<SocketCommandContext>
    {
        // ~ejemplo doble 20 -> 40
        [Command("duplicar")]
        [Summary("Duplica un numero.")]
        public async Task DoubleAsync([Summary("El numero a duplicar.")] int num)
        {
            // Tambien se puede acceder al canal desde el Command Context.
            await Context.Channel.SendMessageAsync($"{num}*2 = {num * 2}");
        }

        // ~sample userinfo --> foxbot#0282
        // ~sample userinfo @Khionu --> Khionu#8708
        // ~sample userinfo Khionu#8708 --> Khionu#8708
        // ~sample userinfo Khionu --> Khionu#8708
        // ~sample userinfo 96642168176807936 --> Khionu#8708
        // ~sample whois 96642168176807936 --> Khionu#8708

        [Command("infodeusuario")]
        [Summary("Retorna la informacion del usuario actual o el usuario de parametro si pasan uno.")]
        [Alias("user","whois")]
        public async Task UserInfoAsync([Summary("El usuario (opcional) para mostrar la informacion.")] SocketUser user = null)
        {
            var userInfo = user ?? Context.Client.CurrentUser;
            await ReplyAsync($"{userInfo.Username}#{userInfo.Discriminator}");
        }
    }
}
